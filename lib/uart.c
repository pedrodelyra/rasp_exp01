#ifndef __UART__
#define __UART__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <include/uart.h>

int receive_response(int request_type, int uart, void* result);

int build_request_message(unsigned char request_type, char* request_parameter, char* request_message);

int build_result(unsigned char request_type, char* response_message, void* result);

void print_request_message(char* message, int size);

int set_uart_configurations(int uart_fildes) {
	struct termios uart_options;
	tcgetattr(uart_fildes, &uart_options); 
	uart_options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;  
	uart_options.c_iflag = IGNPAR; 
	uart_options.c_oflag = 0; 
	uart_options.c_lflag = 0; 
	tcflush(uart_fildes, TCIFLUSH); 
	tcsetattr(uart_fildes, TCSANOW, &uart_options);
}

int open_uart() {
	int uart_file = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
	if(uart_file == -1) {
		perror("Error while openning uart");
		exit(EXIT_FAILURE);
	}

	set_uart_configurations(uart_file);
	return uart_file;
}

int send_request(unsigned char request_type, char* request_parameter, void* result) {
	char request_message[256];
	int request_message_size;
	int uart = open_uart();
	if((request_message_size = build_request_message(request_type, request_parameter, request_message)) == -1) {
		fprintf(stderr, "Unrecogneized request. Program aborted.\n");
		close(uart);
		exit(EXIT_FAILURE);
	}

	print_request_message(request_message, request_message_size);
	printf("Sending request (0x%X)...\n", request_type);
	if(write(uart, request_message, request_message_size) == -1) {
		perror("Error while sending request");
		close(uart);
		return -1;
	} else {
		printf("Request sent.\n");
		printf("Waiting for response...\n");
		receive_response(request_type, uart, result);
	}
	
	close(uart);
	return 0;
}

int receive_response(int request_type, int uart, void* result) {
	char response_message[256];
	int response_message_size;
	sleep(2);
	if((response_message_size = read(uart, response_message, 256)) == -1) {
		perror("Error while receiving request's response");
		return -1;
	} else {
		printf("Respost received.\n");
		build_result(request_type, response_message, result);
	}
}

int build_request_message(unsigned char request_type, char* request_parameter, char* request_message) {
	int message_size;
	switch(request_type) {
		case READ_INTEGER_CODE:
		case READ_FLOAT_CODE:
		case READ_STRING_CODE:
			sprintf(request_message, "%c5725", request_type);
			message_size = 5;
			break;
		case WRITE_INTEGER_CODE:;
			int int_parameter;
			int_parameter = atoi(request_parameter);
			printf("Request Parameter: %d\n", int_parameter);

			request_message[0] = request_type;
			memcpy(request_message + 1, &int_parameter, 4);
			memcpy(request_message + 5, "5725", 4);
			message_size = 9;
			break;
		case WRITE_FLOAT_CODE:;
			float float_parameter;
			float_parameter = atof(request_parameter);
			printf("Request Parameter: %f\n", float_parameter);

			request_message[0] = request_type;
			memcpy(request_message + 1, &float_parameter, 4);
			memcpy(request_message + 5, "5725", 4);
			message_size = 9;
			break;
		case WRITE_STRING_CODE:
			printf("Request Parameter: %s$\n", request_parameter);
			request_message[0] = request_type;
			request_message[1] = strlen(request_parameter);
			memcpy(request_message + 2, request_parameter, request_message[1]);
			memcpy(request_message + 2 + request_message[1], "5725", 4);
			message_size = 6 + request_message[1];
			break;
		default:
			message_size = -1;
	}

	return message_size;
}

int build_result(unsigned char request_type, char* response_message, void* result) {
	switch(request_type) {
		case READ_INTEGER_CODE:;
		case READ_FLOAT_CODE:;
		case WRITE_INTEGER_CODE:;
		case WRITE_FLOAT_CODE:;
			int i;
			for(i = *(long int *) result = 0; i < 4; ++i) {
				*(long int *) result <<= 8;
				*(long int *) result |= response_message[3 - i];
			}
			break;
		case READ_STRING_CODE:;
			int result_size = response_message[0];
			memcpy(result, response_message + 1, result_size);
			*((char *) result + result_size) = '\0';
			break;
		case WRITE_STRING_CODE:;
			*(char *) result = response_message[0];
			*((char *) result + 1) = '\0';
			break;
	}
}

void print_request_message(char* message, int size) {
	int i;
	printf("Request message (%d bytes): ", size);
	for(i = 0; i < size; ++i) {
		if(isalnum(message[i])) {
			printf("%c", message[i]);
		} else {
			printf("(%d)", message[i]);
		}
	}
	printf("\n");
}

#endif
