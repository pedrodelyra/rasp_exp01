#ifndef __MENU__
#define __MENU__
#include <stdio.h>
#include <stdlib.h>
#include <include/uart.h>

void display_menu(int user_option) {
	int input_data_type;
	char user_input[256];
	printf("Select data type to be %s:\n", user_option == 1 ? "read" : "sent");
	printf("1. Integer\t2. Float\t3. String\n");
	scanf("%d", &input_data_type);
	if(user_option == 2) {
		printf("Enter %s: ", input_data_type == 1 ? "integer" : (input_data_type == 2 ? "float" : "string"));
		scanf(" %s", user_input);
	}

	switch(input_data_type) {
		case 1:;
			int integer_result;
			if(send_request(user_option == 1 ? READ_INTEGER_CODE : WRITE_INTEGER_CODE, user_option == 1 ? NULL : user_input, &integer_result) == 0)
				printf("Data received: %d\n", integer_result);
			break;
		case 2:;
			float float_result;
			if(send_request(user_option == 1 ? READ_FLOAT_CODE : WRITE_FLOAT_CODE, user_option == 1 ? NULL : user_input, &float_result) == 0)
				printf("Data received: %f\n", float_result);
			break;
		case 3:;
			char string_result[256];
			if(send_request(user_option == 1 ? READ_STRING_CODE : WRITE_STRING_CODE, user_option == 1 ? NULL : user_input, string_result) == 0)
				printf("Data received: %s\n", string_result);
			break;
		default:
			fprintf(stderr, "Please, select one of the available options next time.\n");
	}
}

void prompt_input(void) {
	int user_option;
	printf("Choose an option:\n");
	printf("1. Read data\t2. Write Data\t3. Exit\n");
	scanf("%d", &user_option);
	switch(user_option) {
		case 1:
		case 2:
			display_menu(user_option);
			break;
		case 3:
			printf("Bye!\n");
			exit(EXIT_SUCCESS);
		default:
			fprintf(stderr, "Please, select one of the available options.\n");
	}
}

#endif
