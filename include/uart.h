#define READ_INTEGER_CODE 0x0A1
#define READ_FLOAT_CODE 0x0A2
#define READ_STRING_CODE 0x0A3

#define WRITE_INTEGER_CODE 0x0B1
#define WRITE_FLOAT_CODE 0x0B2
#define WRITE_STRING_CODE 0x0B3

int open_uart(void);
int send_request(unsigned char request_type, char* request_parameters, void* result);
